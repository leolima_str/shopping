import {
  calculateSubtotal,
  calculateShipping,
  calculateVoucher,
} from '../utils';

test('#calculateSubtotal', () => {
  const cart = {
    Apple: { amountPurchased: 3, price: 20 },
    Banana: { amountPurchased: 10, price: 10 },
  };

  expect(calculateSubtotal(cart)).toBe(160);
});

test('#calculateShipping', () => {
  const cart = {
    Banana: { amountPurchased: 10, price: 10 },
  };

  expect(calculateShipping(cart)).toBe(30);

  cart.Appple = { amountPurchased: 4, price: 20 };

  expect(calculateShipping(cart)).toBe(30);

  cart.Appple = { amountPurchased: 5, price: 20 };

  expect(calculateShipping(cart)).toBe(37);

  cart.Appple = { amountPurchased: 20, price: 20 };

  expect(calculateShipping(cart)).toBe(0);
});

test('#calculateVoucher', () => {
  const vouchers = {
    '#30OFF': { code: '#30OFF', type: 'percentual', amount: 30.0 },
    '#100DOLLARS': { code: '#100DOLLARS', type: 'fixed', amount: 100.0 },
    '#SHIPIT': {
      code: '#SHIPIT',
      type: 'shipping',
      amount: 0,
      minValue: 300.5,
    },
  };

  const discount30Off = calculateVoucher(vouchers['#30OFF'], 200, 30);
  expect(discount30Off).toStrictEqual({
    subTotal: 140,
    shipping: 30,
    total: 170,
    discount: 60,
  });

  const discount100DOLLARS = calculateVoucher(vouchers['#100DOLLARS'], 200, 30);
  expect(discount100DOLLARS).toStrictEqual({
    subTotal: 200,
    shipping: 30,
    total: 130,
    discount: 100,
  });

  const discountSHIPIT = calculateVoucher(vouchers['#SHIPIT'], 330, 37);
  expect(discountSHIPIT).toStrictEqual({
    subTotal: 330,
    shipping: 0,
    total: 330,
    discount: 'Free Shipping',
  });
});
