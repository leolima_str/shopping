import Product from './Product';
import Alert from '../../../components/common/Alert';
import { Wrapper } from '../../../components/ui-layouts';
import { useOrderDetails } from '../../../contexts/OrderDetails';

export default function Showcase() {
  const { products, error } = useOrderDetails();

  return (
    <Wrapper
      margin="0 24px 0 0"
      flexDirection="row"
      flexWrap="wrap"
      maxWidth="768px"
      justifyContent="center"
      smJustifyContent="space-between"
    >
      {error && <Alert />}
      {(products || []).map((item) => (
        <Product key={item.id} {...item} />
      ))}
    </Wrapper>
  );
}
