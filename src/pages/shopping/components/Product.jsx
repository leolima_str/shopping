import { useOrderDetails } from '../../../contexts/OrderDetails';
import { Wrapper } from '../../../components/ui-layouts';
import { Picture } from '../../../components/ui-pictures';
import { Button } from '../../../components/ui-buttons';
import { Heading, Caption } from '../../../components/ui-typography';

export default function Product({ name, price, available, amountPurchased }) {
  const { updateItemCount } = useOrderDetails();
  const incart = amountPurchased > 0;

  return (
    <Wrapper
      width={incart ? '100%' : 'auto'}
      borderRadius="8px"
      background="neutral100"
      flexDirection={incart ? 'row' : 'column'}
      overflow="hidden"
      margin={incart ? '10px 0' : '16px'}
    >
      <Picture
        background="neutral500"
        src={`/images/${name}.png`}
        alt={name}
        borderRadius="8px"
        maxHeight={incart ? '60px' : '200px'}
      />
      <Wrapper flexGrow="1" padding={incart ? '8px' : '16px'}>
        <Heading level={5} as="h2">
          {name}
        </Heading>
        <Wrapper flexDirection="row" justifyContent="space-between">
          <Caption>$ {!incart ? price : amountPurchased * price}</Caption>
          <Caption>
            {!incart ? `${available} left` : `Quantity: ${amountPurchased}`}
          </Caption>
        </Wrapper>
      </Wrapper>
      {!incart && (
        <Button
          fixed
          disabled={available <= 0}
          aria-label={name}
          onClick={() => updateItemCount(name, -1)}
        >
          buy
        </Button>
      )}
      {incart && (
        <Wrapper width="auto">
          <Button
            size="small"
            disabled={available <= 0}
            aria-label={`${name} plus`}
            onClick={() => updateItemCount(name, -1)}
          >
            +
          </Button>
          <Button
            width="100%"
            size="small"
            disabled={amountPurchased <= 0}
            aria-label={`${name} minus`}
            onClick={() => updateItemCount(name, +1)}
          >
            -
          </Button>
        </Wrapper>
      )}
    </Wrapper>
  );
}
