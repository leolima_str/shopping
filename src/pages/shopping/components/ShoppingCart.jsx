import styled from 'styled-components';
import { useRef } from 'react';
import { useOrderDetails } from '../../../contexts/OrderDetails';
import { Wrapper } from '../../../components/ui-layouts';
import { Heading, Caption } from '../../../components/ui-typography';
import { Button } from '../../../components/ui-buttons';
import Product from './Product';

const CustomWrapper = styled(Wrapper)`
  padding: 16px 10px;
  flex-direction: row;
  justify-content: space-between;
  border-top: 1px solid #a49f9d;
`;

const Input = styled.input`
  border-radius: 8px;
  padding: 16px 10px;
  letter-spacing: 0.1em;
  border: 1px solid #a49f9d;
`;

export default function ShoppingCart() {
  const inputRef = useRef();
  const {
    cart,
    subTotal,
    shipping,
    discount,
    total,
    updateVoucher,
  } = useOrderDetails();

  // console.log({ cart, subTotal });
  return (
    <Wrapper width="fit-content" height="fit-content">
      <Wrapper
        width="auto"
        height="100%"
        background="neutral200"
        borderRadius="8px"
        margin="16px 0 0 0"
      >
        <Heading level={3} as="h2" textAlign="center" margin="8px 0">
          Shopping Cart
        </Heading>
        <Wrapper padding="0 10px">
          {Object.keys(cart).map(
            (item) =>
              cart[item]?.amountPurchased > 0 && (
                <Product
                  key={item}
                  {...cart[item]}
                  amountPurchased={cart[item]?.amountPurchased}
                  inCart
                />
              )
          )}
        </Wrapper>
        <CustomWrapper>
          <Input type="text" ref={inputRef} placeholder="Discount code" />
          <Button
            margin="0 0 0 10px"
            onClick={() => {
              updateVoucher(inputRef.current.value);
            }}
          >
            Apply
          </Button>
        </CustomWrapper>
        <CustomWrapper role="group" aria-label="subtotal">
          <Caption>Subtotal</Caption>
          <Caption>$ {subTotal}</Caption>
        </CustomWrapper>
        <CustomWrapper role="group" aria-label="shipping">
          <Caption>Shipping</Caption>
          <Caption>{shipping > 0 ? `$ ${shipping}` : 'FREE'}</Caption>
        </CustomWrapper>
        <CustomWrapper role="group" aria-label="discount">
          <Caption>Discount</Caption>
          <Caption>$ {discount}</Caption>
        </CustomWrapper>
        <CustomWrapper role="group" aria-label="total">
          <Caption>Total</Caption>
          <Caption>$ {total}</Caption>
        </CustomWrapper>
      </Wrapper>
      <Button margin="40px 0" fixed>
        Checkout
      </Button>
    </Wrapper>
  );
}
