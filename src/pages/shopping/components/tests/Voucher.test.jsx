import { screen, waitFor } from '@testing-library/react';
import { render } from '../../../../test-utils';
import userEvent from '@testing-library/user-event';

import Shopping from '../../index';

describe('voucher tests', () => {
  test('voucher calculation', async () => {
    render(<Shopping />);

    // find elements
    const subTotal = screen.getByRole('group', { name: 'subtotal' });
    const shipping = screen.getByRole('group', { name: 'shipping' });
    const discount = screen.getByRole('group', { name: 'discount' });
    const total = screen.getByRole('group', { name: 'total' });

    // find the buy button of the orange
    const orangeBuyButton = await screen.findByRole('button', {
      name: 'Orange',
    });

    // find the buy button of the orange
    const appleBuyButton = await screen.findByRole('button', {
      name: 'Apple',
    });

    for (let i = 0; i < 10; i++) {
      userEvent.click(appleBuyButton);
    }

    // find the discount input
    const discountInput = await screen.findByPlaceholderText('Discount code');

    // find the apply button
    const applyButton = await screen.findByRole('button', {
      name: 'Apply',
    });

    // #30OFF: percentual voucher of 30%
    userEvent.type(discountInput, '#30OFF');
    userEvent.click(applyButton);

    await waitFor(async () => {
      expect(subTotal).toHaveTextContent('Subtotal$ 140');
      expect(discount).toHaveTextContent('Discount$ 60');
      expect(shipping).toHaveTextContent('Shipping$ 30');
      expect(total).toHaveTextContent('Total$ 170');
    });

    userEvent.clear(discountInput);
    userEvent.type(discountInput, '#100DOLLARS');
    userEvent.click(applyButton);

    await waitFor(async () => {
      expect(subTotal).toHaveTextContent('Subtotal$ 200');
      expect(discount).toHaveTextContent('Discount$ 100');
      expect(shipping).toHaveTextContent('Shipping$ 30');
      expect(total).toHaveTextContent('Total$ 130');
    });

    for (let i = 0; i < 5; i++) {
      userEvent.click(orangeBuyButton);
    }

    userEvent.clear(discountInput);
    userEvent.type(discountInput, '#SHIPIT');
    userEvent.click(applyButton);

    await waitFor(async () => {
      expect(subTotal).toHaveTextContent('Subtotal$ 350');
      expect(discount).toHaveTextContent('Discount$ Free Shipping');
      expect(shipping).toHaveTextContent('ShippingFREE');
      expect(total).toHaveTextContent('Total$ 350');
    });
  });
});
