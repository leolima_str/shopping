import { screen } from '@testing-library/react';
import { render } from '../../../../test-utils';
import userEvent from '@testing-library/user-event';

import Shopping from '../../index';

describe('shopping scart tests', () => {
  test('add or remove product in the cart', async () => {
    render(<Shopping />);

    // find the buy button of the orange
    const orangeBuyButton = await screen.findByRole('button', {
      name: 'Orange',
    });
    userEvent.click(orangeBuyButton);

    // the number of oranges in cart should be updated
    const orangeInCart = await screen.findByText('Quantity: 1');
    expect(orangeInCart).toBeInTheDocument();

    // find the plus button of the orange in the cart
    const orangePlusButton = await screen.findByRole('button', {
      name: 'Orange plus',
    });
    expect(orangePlusButton).toBeInTheDocument();

    // clicking the plus button of the orange in the cart,
    // should increase the amount purchased of the item
    userEvent.click(orangePlusButton);

    expect(orangeInCart).toHaveTextContent('Quantity: 2');

    // find the minus button of the orange in the cart
    const orangeMinusButton = await screen.findByRole('button', {
      name: 'Orange minus',
    });
    expect(orangeMinusButton).toBeInTheDocument();

    // clicking the minus button of the orange in the cart,
    // should remove it from the cart if the amount purchased is zero
    userEvent.click(orangeMinusButton);
    userEvent.click(orangeMinusButton);

    expect(orangeMinusButton).not.toBeInTheDocument();
  });

  test('calculate subtotal when adding or removing items from the cart', async () => {
    render(<Shopping />);

    // find the subtotal
    const subTotal = screen.getByRole('group', { name: 'subtotal' });

    // find the buy button of the orange
    const orangeBuyButton = await screen.findByRole('button', {
      name: 'Orange',
    });
    userEvent.click(orangeBuyButton);
    userEvent.click(orangeBuyButton);

    expect(subTotal).toHaveTextContent('Subtotal$ 60');

    // find the minus button of the orange in the cart
    const orangeMinusButton = await screen.findByRole('button', {
      name: 'Orange minus',
    });
    expect(orangeMinusButton).toBeInTheDocument();
    userEvent.click(orangeMinusButton);

    expect(subTotal).toHaveTextContent('Subtotal$ 30');
  });

  test('shipping calculation', async () => {
    render(<Shopping />);

    // find the shipping
    const shipping = screen.getByRole('group', { name: 'shipping' });

    // find the buy button of the apple
    const appleBuyButton = await screen.findByRole('button', {
      name: 'Apple',
    });
    for (let i = 0; i < 9; i++) {
      userEvent.click(appleBuyButton);
    }
    // after add 9 apples to cart, the shipping should be 30
    expect(shipping).toHaveTextContent('Shipping$ 30');

    userEvent.click(appleBuyButton);

    // For purchases below or equal 10kg the shipping price is: $30.
    expect(shipping).toHaveTextContent('Shipping$ 30');

    // find the buy button of the mango
    const mangoBuyButton = await screen.findByRole('button', {
      name: 'Mango',
    });

    // buy more 5 mangos
    for (let i = 0; i < 5; i++) {
      userEvent.click(mangoBuyButton);
    }

    // Each 5kg above 10kg will add $7 to the shipping price.
    expect(shipping).toHaveTextContent('Shipping$ 37');

    // buy more 5 mangos
    for (let i = 0; i < 5; i++) {
      userEvent.click(mangoBuyButton);
    }

    // Each 5kg above 10kg will add $7 to the shipping price.
    expect(shipping).toHaveTextContent('Shipping$ 44');

    // buy more 5 mangos
    for (let i = 0; i < 5; i++) {
      userEvent.click(mangoBuyButton);
    }

    // For purchases above R$400.00 the shipping is free!
    expect(shipping).toHaveTextContent('ShippingFREE');
  });
});
