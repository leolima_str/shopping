import { screen } from '@testing-library/react';
import { render } from '../../../../test-utils';
import userEvent from '@testing-library/user-event';

import Showcase from '../Showcase';

describe('product tests', () => {
  test('click on a buy button, should update the remaining quantity', async () => {
    render(<Showcase />);

    // find the quantity of oranges available
    const availableOranges = await screen.findByText('8 left');
    expect(availableOranges).toBeInTheDocument();

    // find the orange buy button
    const orangeBuyButton = await screen.findByRole('button', {
      name: 'Orange',
    });
    userEvent.click(orangeBuyButton);

    // the number of available oranges should be updated
    expect(availableOranges).toHaveTextContent('7 left');

    userEvent.click(orangeBuyButton);
    userEvent.click(orangeBuyButton);
    userEvent.click(orangeBuyButton);
    userEvent.click(orangeBuyButton);
    userEvent.click(orangeBuyButton);
    userEvent.click(orangeBuyButton);
    userEvent.click(orangeBuyButton);
    userEvent.click(orangeBuyButton);

    // we expect that a product becomes unavailable if its out of stock
    expect(availableOranges).toHaveTextContent('0 left');

    // the buy button should be disabled if it's out of stock
    expect(orangeBuyButton).toBeDisabled();
  });
});
