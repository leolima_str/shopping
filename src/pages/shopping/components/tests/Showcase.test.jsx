import { screen } from '@testing-library/react';
import { rest } from 'msw';
import { render } from '../../../../test-utils';
import { server } from '../../../../mocks/server';
import { urlProducts } from '../../../../config/constants';

import Showcase from '../Showcase';

test('list successfully the products from server', async () => {
  render(<Showcase />);

  // find images
  const productsImages = await screen.findAllByRole('img');
  expect(productsImages).toHaveLength(4);

  // check alt text of images
  const altText = productsImages.map((e) => e.alt);
  expect(altText).toEqual(['Banana', 'Apple', 'Orange', 'Mango']);
});

test('handle error when requesting products', async () => {
  server.resetHandlers([
    rest.get(`${process.env.REACT_APP_API}${urlProducts}`, (_req, res, ctx) => {
      res(ctx.status(500));
    }),
  ]);

  render(<Showcase />);

  const alert = await screen.findByRole('alert', {
    name: 'An unexpected error has occurred. Please try again later.',
  });
  expect(alert).toBeInTheDocument();
});
