import ShoppingCart from './components/ShoppingCart';
import Showcase from './components/Showcase';
import { Wrapper } from '../../components/ui-layouts';
import { Heading } from '../../components/ui-typography';
import { Picture } from '../../components/ui-pictures';

export default function Shopping() {
  return (
    <>
      <Wrapper maxWidth="100%" background="neutral300" margin="0 0 40px 0">
        <Wrapper
          maxWidth="1124px"
          margin="auto"
          flexDirection="row"
          justifyContent="space-between"
          alignItems="center"
        >
          <Heading margin="10px">Shopping</Heading>
          <Wrapper margin="0" width="fit-content" flexDirection="row">
            <Picture
              maxHeight="50px"
              background="neutral500"
              src={`/images/profile.png`}
              alt="profile"
              borderRadius="50%"
            />
            <Heading level={3} margin="10px">
              John Doe
            </Heading>
          </Wrapper>
        </Wrapper>
      </Wrapper>
      <Wrapper maxWidth="1124px" smFlexDirection="row">
        <Showcase />
        <ShoppingCart />
      </Wrapper>
    </>
  );
}
