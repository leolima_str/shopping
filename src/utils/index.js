export function calculateSubtotal(items) {
  let subTotal = 0;
  Object.keys(items).forEach((key) => {
    subTotal += items[key]?.amountPurchased * items[key]?.price;
  });

  return subTotal;
}

export function calculateShipping(items) {
  const valueDefault = 30;
  let subTotal = 0;
  let totalAmount = 0;
  Object.keys(items).forEach((key) => {
    totalAmount += items[key]?.amountPurchased;
    subTotal += items[key]?.amountPurchased * items[key]?.price;
  });

  // For purchases below or equal 10kg the shipping price is: $30.
  if (totalAmount <= 10 && totalAmount > 0) {
    return valueDefault;
  }

  // Each 5kg above 10kg will add $7 to the shipping price.
  if (totalAmount > 10 && subTotal < 400) {
    return valueDefault + Math.floor((totalAmount - 10) / 5) * 7;
  }

  // For purchases above R$400.00 the shipping is free!
  return 0;
}

export function calculateVoucher(voucher, subTotal, shipping) {
  let total = subTotal + shipping;
  let discount = 0;

  if (!voucher) {
    return { subTotal, shipping, total, discount };
  }

  // Percentual voucher: vouchers that reduce an amount
  // in percentage of the cost on subTotal.
  if (voucher.type === 'percentual') {
    discount = (voucher.amount / 100) * subTotal;
    subTotal = subTotal - discount;
    total = subTotal + shipping;
  }

  // Fixed voucher: vouchers with fixed amounts that should reduce over the TOTAL.
  if (voucher.type === 'fixed') {
    discount = voucher.amount;
    total = subTotal + shipping - discount;
  }

  // Free Shipping: make the shipping price become 0 when applied,
  // and should have a minimum subTotal requirement
  if (voucher.type === 'shipping' && subTotal >= voucher.minValue) {
    shipping = 0;
    total = subTotal;
    discount = 'Free Shipping';
  }

  return { subTotal, shipping, total, discount };
}
