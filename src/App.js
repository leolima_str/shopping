import Shopping from './pages/shopping';
import { OrderDetailsProvider } from './contexts/OrderDetails';
import { ThemeProvider } from './components/ui-themes';

function App() {
  return (
    <OrderDetailsProvider>
      <ThemeProvider>
        <Shopping />
      </ThemeProvider>
    </OrderDetailsProvider>
  );
}

export default App;
