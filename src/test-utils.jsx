import React from 'react';
import { render } from '@testing-library/react';
import { ThemeProvider } from './components/ui-themes';
import { OrderDetailsProvider } from './contexts/OrderDetails';

const AllTheProviders = ({ children }) => {
  return (
    <ThemeProvider theme="light">
      <OrderDetailsProvider>{children}</OrderDetailsProvider>
    </ThemeProvider>
  );
};

const customRender = (ui, options) =>
  render(ui, { wrapper: AllTheProviders, ...options });

// re-export everything
export * from '@testing-library/react';

// override render method
export { customRender as render };
