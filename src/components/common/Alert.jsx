import PropTypes from 'prop-types';
import { Wrapper } from '../ui-layouts';
import { Heading } from '../ui-typography';

export default function Alert({ message, variant }) {
  return (
    <Wrapper
      aria-label={message}
      role="alert"
      background={`${variant}Dark`}
      borderRadius="8px"
      padding="16px"
      height="fit-content"
      margin="16px"
      color="errorLight"
    >
      <Heading level={6}>{message}</Heading>
    </Wrapper>
  );
}

Alert.propTypes = {
  variant: PropTypes.oneOf(['success', 'error']),
  message: PropTypes.string,
};

Alert.defaultProps = {
  variant: 'error',
  message: 'An unexpected error has occurred. Please try again later.',
};
