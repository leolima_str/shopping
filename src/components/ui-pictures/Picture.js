import styled from 'styled-components';
import { getProp, getColorByProp, customBaseSizes } from '../ui-themes';

export const Picture = styled.img`
  background: ${getColorByProp('background')};
  border-radius: ${getProp('borderRadius')};

  & {
    ${customBaseSizes}
  }
`;
