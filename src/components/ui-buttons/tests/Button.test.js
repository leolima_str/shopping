import React from 'react';
import { render } from '../../../test-utils';
import { Button } from '../index';

const variations = [
  ['small', 'primary', false, false],
  ['small', 'secondary', false, false],

  ['small', 'primary', true, false],
  ['small', 'secondary', true, false],

  ['small', 'primary', false, true],
  ['small', 'secondary', false, true],

  ['small', 'primary', true, true],
  ['small', 'secondary', true, true],

  ['medium', 'primary', false, false],
  ['medium', 'secondary', false, false],

  ['medium', 'primary', true, false],
  ['medium', 'secondary', true, false],

  ['medium', 'primary', false, true],
  ['medium', 'secondary', false, true],

  ['medium', 'primary', true, true],
  ['medium', 'secondary', true, true],
];

test.each(variations)(
  '<Button size="%s" variant="%s" disabled="%d" fixed="%d">',
  (size, variant, disabled, fixed) => {
    const { container } = render(
      <Button size={size} variant={variant} disabled={disabled} fixed={fixed}>
        Button size={size} variant={variant} negative= disabled={disabled}
      </Button>
    );
    expect(container.firstChild).toMatchSnapshot();
  }
);
