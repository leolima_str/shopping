import styled from 'styled-components';
import PropTypes from 'prop-types';
import is from 'styled-is';
import {
  customBaseSpacing,
  getColorByVariant,
  getValueByMapVariant,
  getValueByMapSize,
  getColor,
  getProp,
} from '../ui-themes';

const variantsMap = ['primary', 'secondary'];
const sizeMap = ['medium', 'small'];

const paddingSize = ['16px', '6.5px 8px'];
const fontSize = ['16px', '14px'];
const borderSize = ['0px', '1px'];
const borderRadius = ['8px', '6px'];

const bgVariant = ['neutral900', 'neutral300'];
const borderColorVariant = ['neutral900', 'neutral300'];
const bgHoverVariant = ['neutral700', 'neutral300'];
const bgActiveVariant = ['neutral500', 'neutral300'];
const fontColorVariant = ['neutral0', 'neutral900'];

export const Button = styled.button`
  display: inline-block;
  width: ${getProp('width')};
  padding: ${getValueByMapSize(sizeMap, paddingSize)};
  text-decoration: none;
  text-transform: uppercase;
  box-sizing: border-box;
  outline: none;
  cursor: pointer;
  color: ${getColorByVariant(variantsMap, fontColorVariant)};
  border-radius: ${getValueByMapSize(sizeMap, borderRadius)};
  border: ${getValueByMapVariant(variantsMap, borderSize)} solid
    ${getColorByVariant(variantsMap, borderColorVariant)};
  background-color: ${getColorByVariant(variantsMap, bgVariant)};
  :hover {
    background-color: ${getColorByVariant(variantsMap, bgHoverVariant)};
  }
  :active {
    background-color: ${getColorByVariant(variantsMap, bgActiveVariant)};
  }

  ${is('disabled')`
    cursor: not-allowed;
    background-color: ${getColor('neutral300')};
    color: ${getColor('neutral500')};
    :hover {
      background-color: ${getColor('neutral300')};
    }
  `};

  ${is('fixed')`
    width: 100%;
  `};

  font-size: ${getValueByMapSize(sizeMap, fontSize)};
  line-height: 125%;
  font-family: 'Sora', sans-serif;
  text-align: center;
  font-style: normal;
  font-weight: bold;

  & {
    ${customBaseSpacing}
  }
`;

Button.propTypes = {
  size: PropTypes.oneOf(sizeMap),
  variant: PropTypes.oneOf(variantsMap),
  fixed: PropTypes.bool,
  width: PropTypes.string,
};

Button.defaultProps = {
  size: 'medium',
  variant: 'primary',
  fixed: undefined,
  width: 'fit-content',
};
