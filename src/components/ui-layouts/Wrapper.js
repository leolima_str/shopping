import styled from 'styled-components';
import {
  customBaseSpacing,
  customBaseDisplay,
  customBaseSizes,
  getProp,
  getColorByProp,
} from '../ui-themes';

export const Wrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  max-width: 1024px;
  margin: 0 auto;
  color: ${getColorByProp('color')};
  background: ${getColorByProp('background')};
  border-radius: ${getProp('borderRadius')};

  & {
    ${customBaseSpacing}
    ${customBaseSizes}
    ${customBaseDisplay}
  }
`;
