export const getColor = (color) => ({ theme }) => {
  return theme && theme.colors ? theme.colors[color] : 'black';
};

export const getProp = (propName, defaultValue = undefined) => (props) =>
  props[propName] || defaultValue;

export const getColorByProp = (propName, defaultValue) => (props) => {
  if (!props[propName]) {
    return defaultValue;
  }

  return props[propName] && props.theme.colors[props[propName]]
    ? props.theme.colors[props[propName]]
    : props[propName];
};

export const getValueByMapVariant = (map, values) => ({ variant }) =>
  values[map.findIndex((s) => s === variant) || 0];

export const getColorByVariant = (map, values) => ({ theme, variant }) => {
  return theme && theme.colors
    ? theme.colors[values[map.findIndex((s) => s === variant) || 0]]
    : 'black';
};

export const getValueByMapSize = (map, values) => ({ size }) =>
  values[map.findIndex((s) => s === size) || 0];
