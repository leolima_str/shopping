import { css } from 'styled-components';
import is from 'styled-is';
import { getProp } from '../utils';

export const customBaseDisplay = css`
  display: ${getProp('display')};
  flex-wrap: ${getProp('flexWrap')};
  flex-grow: ${getProp('flexGrow')};
  flex-direction: ${getProp('flexDirection')};
  justify-content: ${getProp('justifyContent')};
  align-items: ${getProp('alignItems')};

  ${is(`smFlexDirection`)`
    @media (min-width: 768px) {
      flex-direction: ${getProp(`smFlexDirection`)};
    }
  `}

  ${is(`smJustifyContent`)`
    @media (min-width: 768px) {
      justify-content: ${getProp(`smJustifyContent`)};
    }
  `}
`;
