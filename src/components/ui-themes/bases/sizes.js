import { css } from 'styled-components';
import { getProp } from '../utils';

export const customBaseSizes = css`
  height: ${getProp('height')};
  min-height: ${getProp('minHeight')};
  max-height: ${getProp('maxHeight')};

  width: ${getProp('width')};
  min-width: ${getProp('minWidth')};
  max-width: ${getProp('maxWidth')};
`;
