import { css } from 'styled-components';
import { getProp } from '../utils';

// base to decorate components that should receive paddings and margins
export const customBaseSpacing = css`
  padding: ${getProp('padding')};
  margin: ${getProp('margin')};
`;
