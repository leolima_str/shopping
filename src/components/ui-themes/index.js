import React from 'react';
import { ThemeProvider as Theme, createGlobalStyle } from 'styled-components';
import { normalize } from 'styled-normalize';
import { theme } from './light';

const GlobalStyle = createGlobalStyle`
  ${normalize}

  * {box-sizing: border-box;}

  html {
    h1,h2,h3,h4,h5,h6 {
      font-family: 'Sora', sans-serif;    
    }
    p, span, label {
      font-family: 'Rubik', sans-serif;
    }
  }
`;

export const ThemeProvider = ({ children }) => (
  <Theme theme={theme}>
    <GlobalStyle />
    {children}
  </Theme>
);

export * from './bases/spacing';
export * from './bases/display';
export * from './bases/sizes';
export * from './utils';
