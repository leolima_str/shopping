// consider this size for cell phone screens
export const sizes = {
  lg: '1024px',
  sm: '768px',
  xs: '0px',
};
