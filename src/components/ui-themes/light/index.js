import { colors } from './colors';
import { sizes } from './sizes';

export const theme = {
  mode: 'default',
  colors,
  sizes,
};
