// uoltel foundation
// Brand
export const colors = {
  transparent: 'transparent',

  primary900: '#6E1E13',
  primary700: '#99321C',
  primary500: '#EC6833',
  primary300: '#F4A36E',
  primary100: '#F7EADE',
  primary50: '#FBF8F4',

  secondary900: '#383D32',
  secondary700: '#475936',
  secondary500: '#5F713D',
  secondary300: '#839C77',
  secondary100: '#B1C6B2',
  secondary50: '#ECF1ED',

  // Base
  neutral900: '#484747',
  neutral700: '#7B7774',
  neutral500: '#A49F9D',
  neutral300: '#CCC9C7',
  neutral200: '#d8d8d8',
  neutral100: '#E9E8E7',
  neutral50: '#F9F8F7',
  neutral0: '#FFFFFF',

  // Feedback
  warningDark: '#FC900D',
  warningMain: '#FFA600',
  warningLight: '#FFF3DE',
  successDark: '#00644B',
  successMain: '#FF6105',
  successLight: '#20A483',
  infoDark: '#2E4280',
  infoMain: '#4B80C5',
  infoLight: '#BFDBF2',
  errorDark: '#DD445F',
  errorMain: '#E17272',
  errorLight: '#FEEBEE',
};
