import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { customBaseSpacing, getColorByProp, getProp } from '../ui-themes';

const fontSizeDesktop = ['44px', '28px', '24px', '20px', '16px', '14px'];
const letterSpacing = ['0', '0', '0', '0', '0', '0'];

const fontSizeMobile = ['32px', '24px', '22px', '20px', '16px', '14px'];
const max6 = (value) => Math.min(Math.abs(value), 6);

const getValueByLevel = (values) => ({ level }) => values[max6(level - 1)];
const getCorrectHeading = ({ level }) => `h${max6(level)}`;

export const HeadingStyled = styled.h1`
  margin: 0;
  font-size: ${getValueByLevel(fontSizeMobile)};
  font-weight: 600;
  line-height: 125%;
  color: ${getColorByProp('color')};
  text-align: ${getProp('textAlign')};
  letter-spacing: ${getValueByLevel(letterSpacing)};

  @media (min-width: 768px) {
    font-size: ${getValueByLevel(fontSizeDesktop)};
  }

  & {
    ${customBaseSpacing};
  }
`;

export const Heading = (props) => (
  <HeadingStyled as={getCorrectHeading(props)} {...props} />
);

Heading.propTypes = {
  color: PropTypes.string,
  level: PropTypes.oneOf([1, 2, 3, 4, 5, 6]),
  textAlign: PropTypes.oneOf(['left', 'center', 'right', 'justify']),
};

Heading.defaultProps = {
  color: 'inherit',
  level: 1,
  textAlign: 'left',
};
