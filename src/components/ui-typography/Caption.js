import styled from 'styled-components';
import PropTypes from 'prop-types';
import {
  customBaseDisplay,
  customBaseSpacing,
  getColorByProp,
  getProp,
} from '../ui-themes';

export const Caption = styled.span`
  font-size: 14px;
  line-height: 150%;
  letter-spacing: 0.1em;
  color: ${getColorByProp('color')};
  text-align: ${getProp('textAlign')};

  & {
    ${customBaseDisplay}
    ${customBaseSpacing}
  }
`;

Caption.propTypes = {
  color: PropTypes.string,
  textAlign: PropTypes.oneOf(['left', 'center', 'right', 'justify']),
};

Caption.defaultProps = {
  color: 'inherit',
  textAlign: 'left',
};
