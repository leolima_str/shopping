import axios from 'axios';
import { urlVourchers, urlProducts } from '../config/constants';

const api = axios.create({
  baseURL: process.env.REACT_APP_API,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  crossDomain: true,
});

export default api;

export const getProducts = () => api.get(`${urlProducts}`);
export const getVouchers = () => api.get(`${urlVourchers}`);
