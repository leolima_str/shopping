import { createContext, useContext, useState, useEffect, useMemo } from 'react';
import { getProducts, getVouchers } from '../services/api';
import {
  calculateSubtotal,
  calculateShipping,
  calculateVoucher,
} from '../utils';

const OrderDetails = createContext();

export function useOrderDetails() {
  const context = useContext(OrderDetails);

  if (!context) {
    throw new Error(
      'userOrderDetails must be used within an OrderDetailsProvider'
    );
  }

  return context;
}

export function OrderDetailsProvider(props) {
  const [voucher, setVoucher] = useState();
  const [error, setError] = useState(false);
  const [data, setData] = useState({
    products: [],
    cart: {},
    subTotal: 0,
    shipping: 0,
    discount: 0,
    total: 0,
  });

  useEffect(() => {
    let subscribed = true;
    getProducts()
      .then((r) =>
        subscribed ? setData({ products: r?.data?.products, cart: {} }) : null
      )
      .catch((_error) => (subscribed ? setError(true) : null));

    return () => (subscribed = false);
  }, []);

  useEffect(() => {
    const tmpSubTotal = calculateSubtotal(data.cart);
    const tmpShipping = calculateShipping(data.cart);
    const { subTotal, shipping, total, discount } = calculateVoucher(
      voucher,
      tmpSubTotal,
      tmpShipping
    );

    setData((p) => ({ ...p, subTotal, shipping, total, discount }));
  }, [data.cart, voucher]);

  const value = useMemo(() => {
    function updateVoucher(discountCode) {
      getVouchers()
        .then((res) => {
          const vouchers = res?.data?.vouchers || [];
          const selectedVoucher = vouchers.find((v) => v.code === discountCode);
          setVoucher(selectedVoucher);
          setError(false);
        })
        .catch((e) => setError(true));
    }

    function updateItemCount(itemName, amount) {
      const products = [...data.products];
      const item = products.find((p) => p.name === itemName);
      const cart = { ...data.cart };

      if (item.available > 0 || amount > 0) {
        item.available = item.available + amount;
        const amountPurchased =
          (cart[item.name]?.amountPurchased || 0) - amount;

        cart[item.name] = { ...item, amountPurchased };
      }

      setData({ ...data, products, cart });
    }

    return {
      ...data,
      error,
      updateItemCount,
      updateVoucher,
    };
  }, [data, error]);

  return <OrderDetails.Provider value={value} {...props} />;
}
